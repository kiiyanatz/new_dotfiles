set nocompatible              " be iMproved, required

syntax enable

filetype off                  " required

set number

set cursorline

set vb t_vb= 

set clipboard=unnamed

set guifont=Source\ Code\ Pro\ for\ Powerline

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

" Enable utf-8
set encoding=utf-8

set hidden

" Disable tabline
set showtabline=1

set foldmethod=indent

" let g:solarized_termcolors=256
set t_Co=16
set background=light
colorscheme solarized

"Split navigation"
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

noremap <C-Z> :update<CR>
noremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>

" Code folding
nnoremap <space> za


let NERDTreeIgnore = ['\.pyc$', 'env']

let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1

call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'jistr/vim-nerdtree-tabs'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.

" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'

" plugin syntastic
Plugin 'scrooloose/syntastic'

" Javascript linting"
Plugin 'wookiehangover/jshint.vim'

" Emmet for vim
Plugin 'mattn/emmet-vim'

" Python jedi autocomplete
Plugin 'davidhalter/jedi-vim'

" Markdown
Plugin 'suan/vim-instant-markdown'

" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" Avoid a name conflict with L9
Plugin 'user/L9', {'name': 'newL9'}

" File explorer for vim
Plugin 'scrooloose/nerdTree'

" Airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" CtrlSpace
Plugin 'vim-ctrlspace/vim-ctrlspace'

Plugin 'https://github.com/freeo/vim-kalisi'

Plugin 'mhinz/vim-startify'

Plugin 'https://github.com/kien/ctrlp.vim'

" Js Syntax
Plugin 'jelera/vim-javascript-syntax'

" Vim colors
Plugin 'flazz/vim-colorschemes'

" Vim show indent lines
Plugin 'Yggdroot/indentLine'

" Tern for vim 
Plugin 'marijnh/tern_for_vim'

" You complete me
Plugin 'Valloric/YouCompleteMe'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

function! NERDTreeQuit()
  redir => buffersoutput
  silent buffers
  redir END
"                     1BufNo  2Mods.     3File           4LineNo
  let pattern = '^\s*\(\d\+\)\(.....\) "\(.*\)"\s\+line \(\d\+\)$'
  let windowfound = 0

  for bline in split(buffersoutput, "\n")
    let m = matchlist(bline, pattern)

    if (len(m) > 0)
      if (m[2] =~ '..a..')
        let windowfound = 1
      endif
    endif
  endfor

  if (!windowfound)
    quitall
  endif
endfunction
autocmd WinEnter * call NERDTreeQuit()

let NERDTreeIgnore = ['\.pyc$']
